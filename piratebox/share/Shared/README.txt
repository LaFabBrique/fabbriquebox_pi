
	</div>
</div>
<footer id="about">
	<div class="container">
		<p class="to-top"><a href="#header">Haut de page</a></p>
		<p>
			<h2>Licence</h2>
			Sauf mention contraire explicite, tous les contenus de ce dispositif sont mis &agrave; disposition sous les termes de la licence <a>CC-BY</a> <i>(Creative Commons - Attribution)</i>. C'est &agrave; dire que vous &ecirc;tes autoris&eacute; &agrave; partager et adapter ces contenus pour toute utilisation (m&ecirc;me commerciale), &agrave; condition de cr&eacute;diter l'auteur original: <i>"La Fab'Brique et ses contributeurs"</i>.
			<br/>
			Ce qui implique que tous les fichiers qui sont d&eacute;pos&eacute;s sur la Fab'BriqueBox sont de fait plac&eacute;s sous cette m&ecirc;me licence (n'h&eacute;sitez pas &agrave; laisser un message lorsque vous postez un fichier afin que celui-ci puisse vous &ecirc;tre cr&eacute;dit&eacute; de fa&ccedil;on plus personnelle).
		</p>
	</div>
</footer>
